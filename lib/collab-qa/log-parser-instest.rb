module CollabQA
  class Log
    def guess_failed_instest
      tf = @lines.grep(/^-- Tests Failed: /)
      if tf.length > 0
        @reasons = tf[0].split(' ')[3..-1]
        #@result = 'OK' if @reasons == ['inst-old'] or @reasons == ['upgrade-old'] or @reasons == ['pkg-upgrade']
      else
        @reasons = []
      end
      if @data =~ /dpkg: error processing common-lisp-controller/
        @reasons << 'clisp'
      end
      if @data =~ /insserv: Service udev has to be enabled to start service lvm2/
        @reasons << 'udevlvm2'
      end
      if @data =~ /dbconfig-common/
        @reasons << 'dbconfig'
      end
      if @data =~ /WARNING: The following essential packages will be removed./
        @reasons << 'essential'
      end
      if @data =~ /connections on Unix domain socket "\/var\/run\/postgresql/
        @reasons << 'pgsql'
      end
      if @data =~ /Can't connect to local MySQL server through socket/
        @reasons << 'mysql'
      end
      if @data =~ /Modified \(by you or by a script\) since installation./
        @comment = <<EOF
Note that the configuration files were not modified during the test,
so this should not happen.
EOF
        @reasons << 'modified-conffile'
      end
      if @data =~ /File on system created by you or by a script./
        @comment = <<EOF
Note that no configuration files were created during the test,
so this should not happen.
EOF
        @reasons << 'created-conffile'
      end
      if @data =~ /trying to overwrite.*which is also in package.*/
        @reasons << 'overwrite-file'
      end
    end

    def extract_log_instest
      @sum_1l = "XXX"
      @extract = []
      @sum_ml = []
      if @reasons.include?('find-deps')
        g1 = @lines.grep_index(/^-- Finding depends( and recommends)?: FAILED$/)[0]
        g2 = @lines.grep_index(/^-- Result: /)[0]
        @sum_ml = @extract = @lines[g1+1..g2-1]
        @sum_1l = "not installable"
      elsif @reasons.include?('inst-after-deps')
        g1 = @lines.grep_index(/^-- Installing the package after its depends and recommends: FAILED$/)[0]
        g2 = @lines.grep_index(/^-- Result: /)[0]
        @sum_ml = @extract = @lines[g1+1..g2-1]
        @sum_1l = "installation fails"
      elsif @reasons.include?('inst-with-deps')
        g1 = @lines.grep_index(/^-- Installing the package together with its depends, without recommends: FAILED$/)[0]
        g2 = @lines.grep_index(/^-- Result: /)[0]
        @sum_ml = @extract = @lines[g1+1..g2-1]
        @sum_1l = "installation fails"
      elsif @reasons.include?('inst-aptitude')
        g1 = @lines.grep_index(/^-- Installing the package with aptitude: FAILED$/)[0]
        if g1
          lines = @lines[g1+1..-1]
          g2 = lines.grep_index(/^-- /)[0]
          @sum_ml = @extract = lines[0..g2-1]
          @sum_1l = "installation fails with aptitude"
        else
          g1 = @lines.grep_index(/^-- Creating new chroot and installing aptitude: FAILED while installing aptitude$/)[0]
          lines = @lines[g1+1..-1]
          g2 = lines.grep_index(/^-- /)[0]
          @sum_ml = @extract = lines[0..g2-1]
          @sum_1l = "installation fails with aptitude (while installing aptitude)"
        end
      elsif @reasons.include?('inst-with-recs')
        g1 = @lines.grep_index(/^-- Installing the package together with its depends and recommends: FAILED$/)[0]
        lines = @lines[g1+1..-1]
        g2 = lines.grep_index(/^-- /)[0]
        @sum_ml = @extract = lines[0..g2-1]
        @sum_1l = "installation fails"
      elsif @reasons.include?('rm-pkg')
        g1 = @lines.grep_index(/^-- Removing the package: FAILED$/)[0]
        lines = @lines[g1+1..-1]
        g2 = lines.grep_index(/^-- /)[0]
        @sum_ml = @extract = lines[0..g2-1]
        @sum_1l = "package removal fails"
      elsif @reasons.include?('purge-pkg')
        g1 = @lines.grep_index(/^-- Removing all dependencies: OK$/)[0]
        lines = @lines[g1..-1]
        g2 = lines.grep_index(/^-- Now testing upgrade from/)[0]
        if g2.nil?
          g2 = lines.grep_index(/^-- Result: FAILED/)[0]
        end
        @sum_ml = @extract = lines[0..g2-1]
        @sum_1l = "package purge (after dependencies removal) fails"
      elsif @reasons.include?('find-rm-after-upgrade') or @reasons.include?('pkg-upgrade') or @reasons.include?('upgrade-old')
        if @reasons.include?('pkg-upgrade')
          l = @lines.grep(/-- New version is unstable version:/)[0]
          if l =~ /UNINSTALLED/
            @reasons << 'removed-during-upgrade'
          else
            @reasons << 'not-upgraded'
          end
        end
        g1 = @lines.grep_index(/^-- Now testing upgrade from /)[0]
        lines = @lines[g1..-1]
        g2 = lines.grep_index(/^-- Result: FAILED/)[0]
        @sum_ml = @extract = lines[0..g2-1]
        @sum_1l = "package fails to upgrade properly from wheezy"
      end
    end
  end
end
