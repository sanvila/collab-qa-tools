
def to_valid_utf8(str)

  return str if str.valid_encoding?
  # first reencode it
  reencoded = str.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '?')
  return reencoded
end
