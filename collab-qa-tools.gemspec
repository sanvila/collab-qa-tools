# frozen_string_literal: true

changelog = File.join(File.dirname(__FILE__), "debian/changelog")
version = `awk '{print($2); exit}' #{changelog}`.strip
version.gsub!(/[\(\)]/, "")
version.gsub!(/[^0-9a-z]+/, ".")

description = <<-EOF
It contains a set of tools used to scan logs, submit bugs,
and track submitted bugs used to do archive-wide testing.

See https://wiki.debian.org/qa.debian.org/ArchiveTesting
EOF

Gem::Specification.new do |spec|
  spec.name          = "collab-qa-tools"
  spec.version       = version
  spec.authors       = ["Antonio Terceiro"]
  spec.email         = ["terceiro@debian.org"]

  spec.summary       = "Set of tools used for collaborative QA archive testing"
  spec.description   = description
  spec.homepage      = "https://wiki.debian.org/qa.debian.org/ArchiveTesting"
  spec.required_ruby_version = ">= 2.4.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = spec.homepage + "/commits"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir["**/*"] - Dir["debian/**/*"]
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{\Abin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "peach"
  spec.add_dependency "re2"
  spec.add_dependency "liquid"
  spec.add_development_dependency 'minitest'

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
