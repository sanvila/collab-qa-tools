#! /usr/bin/ruby

require 'date'
require 'collab-qa'
require 'optparse'

verbose = false
todofile = ENV["TODOFILE"]
disptime = false
restrict = nil
exclude = nil
template = nil
with_bugs = false
ignore_bugs = false
auto_report = false
bugtype = ENV["BUGTYPE"]
pkglist = []
progname = File::basename($PROGRAM_NAME)
opts = OptionParser::new do |opts|
  opts.program_name = progname
  opts.banner = "Usage: #{progname} [options]"
  opts.separator ""
  opts.separator "Options:"

  opts.on("-v", "--verbose", "Verbose mode") do |v|
    verbose = true
  end

  opts.on("-t", "--TODO FILE", "Only TODO lines from file") do |f|
    todofile = f
  end

  opts.on("-r", "--restrict RE", "Only lines matching RE") do |r|
    restrict = /#{r}/
  end

  opts.on("-x", "--exclude RE", "Exclude lines matching RE") do |x|
    exclude = /#{x}/
  end

  opts.on("-T", "--template FILE", "Use FILE as bug report template") do |t|
    template = t
  end

  opts.on("-o", "--only FILE", "Only packages listed in FILE") do |f|
    pkglist = IO::readlines(f).map { |l| l.chomp }
  end

  opts.on("", "--with-bugs", "Only packages with bugs") do |f|
    with_bugs = true
  end

  opts.on("", "--ignore-bugs", "Do not check for existing bugs (warning: this is not a good idea as it might cause duplicate reports)") do |f|
    ignore_bugs = true
  end

  opts.on("", "--auto-report", "Always generate a txt file with the mail to send. The mail need to be sent with /usr/sbin/sendmail -f lucas@debian.org -t < .mail.0ad") do |f|
    auto_report = true
  end


  opts.on("-b", "--bugtype BUGTYPE", "Use built-in template for the given bug type. Ignored if --template is passed") do |b|
    bugtype = b
  end
end
opts.parse!(ARGV)

if todofile
  pkgs = IO::read(todofile).split(/\n/).grep(/ TODO/).map { |e| e.split(' ')[0] }
  files = []
  glob = Dir::glob("*.{log,build}")
  pkgs.each do |pkg|
    next if pkglist != [] and not pkglist.include?(pkg)
    g = glob.select { |e| e =~ /^#{pkg}(_.*|)\.(log|build)$/ }
    files << g[0] if g[0] != nil
  end
else
  files = Dir::glob("*log")
end

date = ENV['DATE']
if date.nil?
  puts "DATE environment variable not set. e.g export DATE=2011/08/23"
  today = Date.today.strftime("%Y/%m/%d")
  print "Use current date (#{today})? [Y/n] "
  l = STDIN.gets.downcase
  if l == "n"
    exit(1)
  else
    date = today
  end
end

quit = false
files.sort.each_with_index do |file, i|
  break if quit
  puts
  puts "######## #{file} ########"
  log = CollabQA::Log::new(file)
  log.extract_log
  summary = log.oneline_to_s(disptime)
  next if restrict && summary !~ restrict
  next if exclude && summary =~ exclude
  if File::exists?(".bugs.#{log.package}")
    bugs = IO::read(".bugs.#{log.package}").split(/\n/)
  else
    bugs = nil
  end
  if ignore_bugs
    bugs = []
  end
  if with_bugs and bugs == nil
    puts "No bugs file yet, skipping."
    next
  end
  if with_bugs and bugs.empty?
    puts "No bugs found, skipping."
    next
  end
  if !log.extract
    puts "No build failure found, skipping."
    next
  end
  puts "--------- Error:"
  puts log.extract
  puts "----------------"
  puts log.sum_1l
  puts "----------------"
  nbl = log.extract.length
  nbc = log.extract.join("\n").length
  puts "package: #{log.package} (#{i+1}/#{files.length})"
  puts "lines: #{nbl} ; chars: #{nbc}" 
  if nbl > 1000
    puts "TOO MANY LINES (#{nbl})"
  end
  if nbc > 50000
    puts "TOO MANY CHARS (#{nbc})"
  end
  versions = `apt-cache showsrc #{log.package} |grep "^Version:" | awk '{print $2}'`.split
  versions.each do |v|
    if `dpkg --compare-versions #{v} gt #{log.version} ; echo $?`.to_i == 0
      puts "There's a newer version available: #{v} (vs: #{log.version})."
    end
  end
  ex = false
  while not ex
    puts '-' * 72
    puts "s: skip"
    puts "i: ignore this package permanently"
    bugs.each_with_index do |i, j|
      puts "#{j+1}: #{i}"
    end
    puts "r: report new bug"
    puts "f: view full log"
    puts "q: quit"
    puts '-' * 72
    print "Action [s|i" + (1..(bugs.length)).map { |i| "|#{i}" }.join + "|r|f|q]: "
    if auto_report
      l = 'r'
    else
      l = STDIN.gets
    end
    l.chomp!
    if l.to_i != 0
      i = l.to_i
      if i <= bugs.length
        bugnum = bugs[l.to_i-1].split[0]
        todo = IO::read(todofile)
        todo.gsub!(/^#{Regexp::escape(log.package)} (.*) TODO.*$/, "#{log.package} \\1 ##{bugnum}")
        File::open(todofile, "w") do |f|
          f.print todo
        end
        ex = true
      else
        puts "E: invalid number. Choose one in 1-#{bugs.length}"
      end
    elsif l == "i"
      todo = IO::read(todofile)
      todo.gsub!(/^#{Regexp::escape(log.package)} (.*) TODO.*$/, "#{log.package} IGNORE")
      File::open(todofile, "w") do |f|
        f.print todo
      end
      ex = true
    elsif l =~ /^v /
      rest, b = l.split(' ')
      if b.to_i != 0
        bugnum = bugs[b.to_i-1].split[0]
        system("w3m -dump http://bugs.debian.org/#{bugnum}")
      end
    elsif l =~ /^sev /
      rest, b = l.split(' ')
      if b.to_i != 0
        bugnum = bugs[b.to_i-1].split[0]
        todo = IO::read(todofile)
        todo.gsub!(/^#{Regexp::escape(log.package)} (.*) TODO.*$/, "#{log.package} \\1 ##{bugnum}")
        File::open(todofile, "w") do |f|
          f.print todo
        end
        ex = true
        system("bts severity #{bugnum} serious")
      end
    elsif l == 'r'
      File::open(".mail.#{log.package}", "w") do |f|
        if template
          f.puts log.to_mail_with_template(date, ENV["DEBFULLNAME"], ENV['DEBEMAIL'], template, file)
        else
          f.puts log.to_mail(date, ENV['DEBFULLNAME'], ENV['DEBEMAIL'], bugtype, file)
        end
      end
      if not auto_report
        cmd = "/usr/bin/mutt -e 'set autoedit' -H .mail.#{log.package}"
        system(cmd)
      end
      if todofile
        print "edit #{todofile}? [Y/n] "
        if auto_report
          l = 'y'
        else
          l = STDIN.gets
          l.chomp!
        end
        if l != 'n'
          todo = IO::read(todofile)
          todo.gsub!(/^#{Regexp::escape(log.package)} (.*) TODO.*$/, "#{log.package} \\1 NNN")
          File::open(todofile, "w") do |f|
            f.print todo
          end
        end
      end
      ex = true
    elsif l == "s"
      puts "Skipping ..."
      ex = true
    elsif l == "f"
      system(ENV["PAGER"] || "less", file)
    elsif l == "q"
      ex = quit = true
    elsif l =~ /^v/
      bugnum = bugs[l[1..-1].to_i-1].split[0]
      system("epiphany http://bugs.debian.org/#{bugnum} &>/dev/null &")
    else
      puts "Unknown command: #{l.inspect}"
    end
  end
end
